<?php 

require_once $_SERVER['DOCUMENT_ROOT']."/helpers/Config.php";

require_once $_SERVER['DOCUMENT_ROOT']."/models/Pagina.php";

checarLogado();

function index(){
    $pagina = listarPagina();
    return $pagina;
}

function visualizar($id){
    $pagina = buscarPagina($id);
    return $pagina;
}

function cadastrar(){

    $pagina = [];

    if(!empty($_POST)){

        $pagina['titulo'] = $_POST['titulo'];
        $pagina['slug'] = $_POST['slug'];
        $pagina['descricao'] = $_POST['descricao'];

        if(cadastrarPaginas($pagina)){

            header('location:/admin/pagina');
            exit;

        }

    }

}

function editar($id){

    $pagina = buscarPagina($id);

    //print_r($plano);

    if(!empty($_POST)){

        $pagina['titulo'] = $_POST['titulo'];
        $pagina['slug'] = $_POST['slug'];
        $pagina['descricao'] = $_POST['descricao'];

        if(editarPaginas($pagina, $id)){
            header('location:/admin/pagina');
            exit;
        }
    }

    return $pagina;
}

function deletar($id){

    if(deletarPagina($id)){
        header('Location:/admin/pagina');
        exit;
    }
}